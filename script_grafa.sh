#!/bin/bash
#https://techexpert.tips/fr/grafana-fr/installation-de-grafana-sur-ubuntu-linux/
#Step 1:utilisez les commandes suivantes pour installer le service de base de données MySQL.
sudo apt-get update
sudo apt-get install mysql-server -y
#Utilisez la commande suivante pour accéder à la ligne de commande MySQL.

mysql -s <<EOF
CREATE DATABASE grafana CHARACTER SET UTF8 COLLATE UTF8_BIN;
CREATE USER 'grafana'@'%' IDENTIFIED BY 'grafana12345';
GRANT ALL PRIVILEGES ON grafana.* TO 'grafana'@'%';
exit;
EOF
#Utilisez les commandes suivantes pour configurer le référentiel Grafana APT.
mkdir /downloads/grafana -p
cd /downloads/grafana
wget https://packages.grafana.com/gpg.key
sudo apt-key add gpg.key
sudo add-apt-repository 'deb [arch=amd64,i386] https://packages.grafana.com/oss/deb stable main'
sudo apt-get update
#Utilisez la commande suivante pour installer Grafana.
sudo apt-get install grafana
#Editez le fichier de configuration Grafana grafana.ini.
#sudo chmod 777 /etc/grafana/grafana.ini
#sudo rm /etc/grafana/grafana.ini
#sudo mv grafana.ini /etc/grafana/
#sudo chmod 640 /etc/grafana/grafana.ini
#Utilisez la commande suivante pour démarrer le service Grafana.
sudo /bin/systemctl start grafana-server
sudo /bin/systemctl daemon-reload
#Le service Grafana commencera à écouter sur le port TCP 3000.
#Utilisez la commande suivante pour voir les journaux Grafana et vérifier si tout fonctionne.
#cat /var/log/grafana/grafana.log
#sudo /bin/systemctl daemon-reload
sudo /bin/systemctl enable grafana-server
sudo /bin/systemctl restart grafana-server
echo "Connexion au tableau de bord Grafana IP:3000"
echo "Entrez les informations de connexion du mot de passe Grafana par défaut Nom d'utilisateur: admin Mot de passe: admin"

